FROM openjdk:11-jdk-oraclelinux8
LABEL maintainer="pucminas.com"

VOLUME /tmp
EXPOSE 8081

ARG JAR_FILE=target/auth-service*.jar
ADD ${JAR_FILE} auth-service.jar

ENTRYPOINT ["sh", "-c", "java -jar /auth-service.jar"]

