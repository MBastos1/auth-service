package com.pucminas.authservice.domain.service

import com.pucminas.authservice.provider.RegistryServiceProvider
import com.pucminas.authservice.web.service.PersonService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class PersonServiceImpl(
    private val registryServiceProvider: RegistryServiceProvider
) : PersonService {
    override fun loadPersonByUserIdAndType(userId: String, type: String) =
        when (type) {
            "A" -> registryServiceProvider.findAssociateByUserId(userId)
            "P" -> registryServiceProvider.findProviderByUserId(userId)
            else -> throw ResponseStatusException(HttpStatus.NOT_FOUND, "Pessoa não encontrada.")
        }
}