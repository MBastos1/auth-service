package com.pucminas.authservice.domain.service

import com.pucminas.authservice.domain.entity.Profile
import com.pucminas.authservice.domain.repository.ProfileRepository
import com.pucminas.authservice.web.service.ProfileService
import org.springframework.stereotype.Service
import java.util.*

@Service
class ProfileServiceImpl(private val profileRepository: ProfileRepository) : ProfileService {

    override fun findById(id: String): Optional<Profile> {
        return profileRepository.findById(id)
    }

    override fun listAll(): MutableList<Profile> {
        return profileRepository.findAll()
    }
}