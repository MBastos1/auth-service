package com.pucminas.authservice.domain.service

import com.pucminas.authservice.common.extension.toDomain
import com.pucminas.authservice.domain.entity.User
import com.pucminas.authservice.domain.repository.UserRepository
import com.pucminas.authservice.web.dto.request.UserRequest
import com.pucminas.authservice.web.dto.response.UserResponse
import com.pucminas.authservice.web.extension.toResponse
import com.pucminas.authservice.web.service.UserService
import javassist.NotFoundException
import org.springframework.stereotype.Service

@Service
class UserServiceImpl(private val userRepository: UserRepository) : UserService {

    override fun findByEmail(email: String): UserResponse =
        userRepository.findByEmail(email).map {
            it.toResponse()
        }.orElseThrow {
            NotFoundException("Usuário ($email) não encontrado.")
        }

    override fun findById(userId: String): UserResponse? =
        userRepository.findById(userId).map {
            it.toResponse()
        }.orElseThrow {
            NotFoundException("Usuário ($userId) não encontrado.")
        }

    override fun save(user: User): UserResponse = userRepository.save(user).toResponse()

    override fun edit(userId: String, userRequest: UserRequest): UserResponse =
        userRepository.findById(userId).map {
            userRepository.save(userRequest.toDomain(userId)).toResponse()
        }.orElseThrow {
            NotFoundException("Usuário ($userId) não encontrado.")
        }

    override fun findAll(): List<UserResponse>? =
        userRepository.findAll().map {
            it.toResponse()
        }
}
