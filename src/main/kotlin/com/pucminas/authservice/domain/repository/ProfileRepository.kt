package com.pucminas.authservice.domain.repository

import com.pucminas.domain.base.BaseRepository
import com.pucminas.authservice.domain.entity.Profile
import org.springframework.stereotype.Repository

@Repository
interface ProfileRepository : BaseRepository<Profile, String> {
}