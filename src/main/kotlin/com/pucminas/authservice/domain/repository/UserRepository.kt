package com.pucminas.authservice.domain.repository

import com.pucminas.authservice.domain.entity.User
import com.pucminas.domain.base.BaseRepository
import java.util.*
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : BaseRepository<User, String> {
    fun findByEmail(email: String): Optional<User>
    fun findByEmailAndPassword(email: String, password: String): User?
}