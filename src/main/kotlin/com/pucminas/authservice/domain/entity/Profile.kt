package com.pucminas.authservice.domain.entity

import com.pucminas.authservice.domain.base.BaseEntity
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class Profile(
    @Id
    @Column(updatable = false, nullable = false, length = 36)
    val id: String,

    @Column(length = 50, nullable = false, insertable = false)
    val description: String? = null,

    @Column(length = 50, nullable = true, insertable = false)
    val detail: String? = null
)