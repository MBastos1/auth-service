package com.pucminas.authservice.domain.entity

import com.pucminas.authservice.domain.base.BaseEntity
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "users")
data class User(

    @Id
    @Column(updatable = false, nullable = false, length = 36)
    override val id: String = UUID.randomUUID().toString(),

    @Column(length = 150, nullable = false)
    val email: String,

    @Column(length = 200, nullable = false)
    var password: String,

    val status: Boolean = true,

    val manager: Boolean = false,

    @Column(length = 200, nullable = true)
    val note: String? = null,

    @Column(nullable = true)
    val lastAccessAt: LocalDateTime? = null,

    @OneToOne
    val profile: Profile

) : BaseEntity()