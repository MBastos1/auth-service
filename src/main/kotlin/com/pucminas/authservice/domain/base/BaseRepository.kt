package com.pucminas.domain.base

import java.io.Serializable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.NoRepositoryBean

@NoRepositoryBean
interface BaseRepository<T, ID : Serializable?> : JpaRepository<T, ID>