package com.pucminas.authservice.domain.base

import java.util.*

interface BaseService<T, ID> {
    fun findById(id: ID): Optional<T>
    fun listAll(): MutableList<T>
}