package com.pucminas.authservice.auth.security.jwt

import com.fasterxml.jackson.annotation.JsonIgnore
import com.pucminas.authservice.web.dto.response.PersonResponse
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

data class JwtUser(
    @get:JsonIgnore val id: String,
    private val username: String,
    private val password: String?,
    val manager: Boolean,
    val email: String,
    val personResponse: PersonResponse? = null,
    private val authorities: Collection<GrantedAuthority?>
) : UserDetails {

    override fun getUsername() = username

    @JsonIgnore
    override fun isAccountNonExpired() = true

    @JsonIgnore
    override fun isAccountNonLocked() = true

    @JsonIgnore
    override fun isCredentialsNonExpired() = true

    @JsonIgnore
    override fun getPassword() = password

    override fun getAuthorities() = authorities

    override fun isEnabled() = true
}