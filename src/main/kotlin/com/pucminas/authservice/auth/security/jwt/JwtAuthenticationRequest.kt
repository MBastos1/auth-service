package com.pucminas.authservice.auth.security.jwt

import org.apache.logging.log4j.util.Strings

class JwtAuthenticationRequest(
    val email: String = Strings.EMPTY,
    var password: String = Strings.EMPTY,
    val personType: String = Strings.EMPTY
)