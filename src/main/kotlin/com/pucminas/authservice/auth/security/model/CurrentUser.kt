package com.pucminas.authservice.auth.security.model

import com.pucminas.authservice.web.dto.response.UserResponse

data class CurrentUser(
    var token: String?,
    var user: UserResponse?
)
