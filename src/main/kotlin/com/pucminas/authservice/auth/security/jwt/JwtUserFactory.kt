package com.pucminas.authservice.auth.security.jwt

import com.pucminas.authservice.web.dto.response.UserResponse
import org.apache.tomcat.jni.User
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority

object JwtUserFactory {
    @JvmStatic
    fun create(user: UserResponse) =
        JwtUser(
            id = user.id,
            username = user.email,
            password = user.password,
            manager = user.manager,
            email = user.email,
            authorities = listOf<GrantedAuthority>(SimpleGrantedAuthority(user.profile.description))
        )
}