package com.pucminas.authservice.auth.security.service

import com.pucminas.authservice.auth.security.jwt.JwtUserFactory
import com.pucminas.authservice.web.service.UserService
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service

@Service
class JwtUserDetailsServiceImpl(private val userService: UserService) : UserDetailsService {

    override fun loadUserByUsername(email: String) =
        userService.findByEmail(email)?.let {
            JwtUserFactory.create(it)
        }
}