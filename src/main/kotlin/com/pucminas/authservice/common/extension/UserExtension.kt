package com.pucminas.authservice.common.extension

import com.pucminas.authservice.domain.entity.Profile
import com.pucminas.authservice.domain.entity.User
import com.pucminas.authservice.web.dto.request.UserRequest
import java.util.*

fun UserRequest.toDomain(id: String?) = User(
    id = id ?: UUID.randomUUID().toString(),
    email = this.email,
    password = this.password,
    manager = this.manager,
    note = this.note,
    profile = Profile(
        id = this.profileId
    )
)