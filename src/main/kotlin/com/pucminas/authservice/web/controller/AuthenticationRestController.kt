package com.pucminas.authservice.web.controller

import com.pucminas.authservice.auth.security.jwt.JwtAuthenticationRequest
import com.pucminas.authservice.auth.security.jwt.JwtTokenUtil
import com.pucminas.authservice.auth.security.model.CurrentUser
import com.pucminas.authservice.web.service.PersonService
import com.pucminas.authservice.web.service.UserService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
class AuthenticationRestController(
    private val authenticationManager: AuthenticationManager,
    private val jwtTokenUtil: JwtTokenUtil,
    private val userDetailsService: UserDetailsService,
    private val userService: UserService,
    private val personService: PersonService
) {

    @PostMapping(value = ["/auth"])
    @ResponseStatus(HttpStatus.OK)
    fun createAuthenticationToken(@RequestBody authenticationRequest: JwtAuthenticationRequest) =
        kotlin.runCatching {
            SecurityContextHolder.getContext().authentication = authenticationManager.authenticate(
                UsernamePasswordAuthenticationToken(
                    authenticationRequest.email,
                    authenticationRequest.password
                )
            )
            val userDetails = userDetailsService.loadUserByUsername(authenticationRequest.email)
            val token = jwtTokenUtil.generateToken(userDetails)
            val user = userService.findByEmail(authenticationRequest.email)?.apply {
                this.person = personService.loadPersonByUserIdAndType(this.id, authenticationRequest.personType)
            }
            CurrentUser(token, user)
        }.getOrElse {
            it.printStackTrace()
            ResponseEntity.status(HttpStatus.UNAUTHORIZED).body<Any>(it.message)
        }
}