package com.pucminas.authservice.web.controller

import com.pucminas.authservice.web.service.ProfileService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(ProfileRestController.URL)
class ProfileRestController(private val profileService: ProfileService) {

    companion object {
        const val URL = "/profile"
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun listAll() =
        profileService.listAll()

    @GetMapping("/{profileId}")
    @ResponseStatus(HttpStatus.OK)
    fun findById(@PathVariable profileId: String) =
        profileService.findById(profileId)
}