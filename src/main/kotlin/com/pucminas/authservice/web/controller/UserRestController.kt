package com.pucminas.authservice.web.controller

import com.pucminas.authservice.common.extension.toDomain
import com.pucminas.authservice.web.dto.request.UserRequest
import com.pucminas.authservice.web.service.UserService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(UserRestController.URL)
class UserRestController(private val userService: UserService) {

    companion object {
        const val URL = "/user"
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun save(@RequestBody userRequest: UserRequest) =
        userService.save(userRequest.toDomain(null))

    @PutMapping("/{userId}")
    @ResponseStatus(HttpStatus.OK)
    fun edit(@PathVariable userId: String, @RequestBody userRequest: UserRequest) =
        userService.edit(userId, userRequest)

    @GetMapping("/{userId}")
    @ResponseStatus(HttpStatus.OK)
    fun findById(@PathVariable userId: String) =
        userService.findById(userId)

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun findAll() =
        userService.findAll()
}