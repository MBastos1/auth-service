package com.pucminas.authservice.web.handler

import feign.FeignException
import javassist.NotFoundException
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@RestControllerAdvice
@RequestMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
class ExceptionHandler : ResponseEntityExceptionHandler() {
    @ExceptionHandler(FeignException::class)
    fun handleFeignStatusException(e: FeignException): ResponseEntity<String> {
        val httpStatus = HttpStatus.valueOf(e.status())
        return ResponseEntity(httpStatus)
    }

    @ExceptionHandler(NotFoundException::class)
    fun handleExceptionStatusException(e: Exception): ResponseEntity<String> {
        return ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
    }
}

