package com.pucminas.authservice.web.service

import com.pucminas.authservice.domain.entity.User
import com.pucminas.authservice.web.dto.request.UserRequest
import com.pucminas.authservice.web.dto.response.UserResponse

interface UserService {
    fun findByEmail(email: String): UserResponse?
    fun findById(userId: String): UserResponse?
    fun save(user: User): UserResponse
    fun edit(userId: String, userRequest: UserRequest): UserResponse
    fun findAll(): List<UserResponse>?
}