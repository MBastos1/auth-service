package com.pucminas.authservice.web.service

import com.pucminas.authservice.domain.base.BaseService
import com.pucminas.authservice.domain.entity.Profile
import org.springframework.stereotype.Service

interface ProfileService : BaseService<Profile, String>