package com.pucminas.authservice.web.service

import com.pucminas.authservice.web.dto.response.PersonResponse

interface PersonService {
    fun loadPersonByUserIdAndType(userId: String, type: String): PersonResponse
}