package com.pucminas.authservice.web.dto.request

data class UserRequest(
    val email: String,
    val password: String,
    val manager: Boolean = false,
    val note: String? = null,
    val profileId: String
)