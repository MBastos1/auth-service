package com.pucminas.authservice.web.dto.response

class AssociateResponse(
    val id: String,
    val cardNumber: String?,
    val holder: Boolean?,
    val dependent: Boolean?,
    val note: String?,
    val person: PersonResponse
)
