package com.pucminas.authservice.web.dto.response

data class ProfileResponse(
    val id: String?,
    val description: String?
)
