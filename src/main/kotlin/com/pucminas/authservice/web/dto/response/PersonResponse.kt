package com.pucminas.authservice.web.dto.response

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
data class PersonResponse(
    val id: String,
    val name: String
)