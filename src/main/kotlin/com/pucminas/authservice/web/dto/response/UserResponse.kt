package com.pucminas.authservice.web.dto.response

import com.fasterxml.jackson.annotation.JsonIgnore

data class UserResponse(
    val id: String,
    @JsonIgnore val name: String? = null,
    val email: String,
    val password: String? = null,
    val manager: Boolean,
    var person: PersonResponse? = null,
    val note: String? = null,
    val profile: ProfileResponse
)
