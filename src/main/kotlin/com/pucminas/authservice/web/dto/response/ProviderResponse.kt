package com.pucminas.authservice.web.dto.response

import java.time.LocalDate

class ProviderResponse(
    val id: String,
    val note: String?,
    val status: Boolean? = true,
    val councilNumber: String?,
    val person: PersonResponse
)
