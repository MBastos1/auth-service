package com.pucminas.authservice.web.extension

import com.pucminas.authservice.domain.entity.User
import com.pucminas.authservice.web.dto.response.ProfileResponse
import com.pucminas.authservice.web.dto.response.UserResponse

fun User.toResponse() = UserResponse(
    id = this.id,
    email = this.email,
    password = this.password,
    manager = this.manager,
    note = this.note,
    profile = ProfileResponse(
        id = this.profile.id,
        description = this.profile.description
    )
)
