package com.pucminas.authservice.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiKey
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spi.service.contexts.SecurityContext
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2


@EnableSwagger2
@Configuration
class SwaggerConfig {

    @Bean
    fun api() = Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.basePackage("com.pucminas.authservice.web"))
        .paths(PathSelectors.any())
        .build()
        .securitySchemes(listOf(apiKey()))
        .apiInfo(apiInfo())

    private fun apiInfo() = ApiInfoBuilder()
        .title("GISA - Auth Service")
        .description("API de autenticação do boa saúde")
        .version("1.0")
        .license("TERMS OF SERVICE URL")
        .licenseUrl("")
        .build()

    private fun apiKey(): ApiKey {
        return ApiKey("jwtToken", "Authorization", "header")
    }
}