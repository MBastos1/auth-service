package com.pucminas.authservice.provider

import com.pucminas.authservice.web.dto.response.PersonResponse
import javassist.NotFoundException
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable

@FeignClient(
    value = "auth",
    url = "http://\${REGISTRY_SERVICE_URL:localhost}:8082"
)
interface RegistryServiceProvider {

    @GetMapping("/api/v1/associate/person/user/{userId}")
    fun findAssociateByUserId(@PathVariable userId: String): PersonResponse

    @GetMapping("/api/v1/provider/person/user/{userId}")
    fun findProviderByUserId(@PathVariable userId: String): PersonResponse
}