-- Script inicial apenas para inicializar o Flyway

-- Tables
CREATE TABLE profile (
	id varchar(36) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	description varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	detail varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	created_at datetime2 NULL,
    updated_at datetime2 NULL,
	CONSTRAINT PK__profile__id PRIMARY KEY (id)
);

CREATE TABLE users (
	id varchar(36) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	email varchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	password varchar(150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	note varchar(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    manager bit default 1,
	status bit default 1,
	profile_id varchar(36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	last_access_at datetime2 NULL,
	created_at datetime2 NULL,
    updated_at datetime2 NULL,
	CONSTRAINT PK__user__id PRIMARY KEY (id),
	CONSTRAINT FK__user__profile FOREIGN KEY (profile_id) REFERENCES profile(id)
);