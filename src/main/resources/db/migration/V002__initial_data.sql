-- Profiles
INSERT INTO profile (id,created_at,updated_at,description,detail)
VALUES('3ba5db7f-6786-4073-9ccf-afcad2267303',getdate(),NULL,'Admin','Perfil de Administrador');

INSERT INTO profile (id,created_at,updated_at,description,detail)
VALUES('853b046d-f6b0-4b95-9de9-b8cad7521baa',getdate(),NULL,'Associado','Perfil de Associado');

INSERT INTO profile (id,created_at,updated_at,description,detail)
VALUES('e692b401-8eaf-4da3-ada6-dde922cba94b',getdate(),NULL,'Prestador','Perfil de Prestador');

-- Users
-- Admin
INSERT INTO users (id,email,password,status,manager,note,profile_id,last_access_at,created_at,updated_at)
VALUES(
    '873ebd63-ade4-481c-8fbd-1eceb5247f14',
    'tccpospuc@gmail.com',
    '$2a$10$27.7C6upPNrCH5/yK9Z9sO/uRCqrBnpYLZGkNCIxCgleSa108DLd.',
    1,
    1,
    NULL,
    '3ba5db7f-6786-4073-9ccf-afcad2267303',
    NULL,
    getdate(),
    NULL
);

-- Associado
INSERT INTO users (id,email,password,status,manager,note,profile_id,last_access_at,created_at,updated_at)
VALUES(
    'bd2a9cbf-f8c0-4ae2-9b18-caa76ae5a0a2',
    'gwyn.alfredsson@gmail.com',
    '$2a$10$27.7C6upPNrCH5/yK9Z9sO/uRCqrBnpYLZGkNCIxCgleSa108DLd.',
    1,
    1,
    NULL,
    '853b046d-f6b0-4b95-9de9-b8cad7521baa',
    NULL,
    getdate(),
    NULL
);

-- Prestador
INSERT INTO users (id,email,password,status,manager,note,profile_id,last_access_at,created_at,updated_at)
VALUES(
    '449671cd-d906-43b8-9bbd-7639c23b188d',
    'bahman.franklyn@gmail.com',
    '$2a$10$27.7C6upPNrCH5/yK9Z9sO/uRCqrBnpYLZGkNCIxCgleSa108DLd.',
    1,
    1,
    NULL,
    '3ba5db7f-6786-4073-9ccf-afcad2267303',
    NULL,
    getdate(),
    NULL
);